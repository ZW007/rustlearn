
use std::io;
use rand::Rng; //trait
use std::cmp::Ordering; //enum type 
fn main() {
    println!("Guess the number!");
    let secret_number = rand::thread_rng().gen_range(1..101);
    println!("randomly generated number is: {}",secret_number);

    loop {
        println!("Please input the number!");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("failed to read line");
        println!("The number you guessed is {}",guess);
        
        // use same name but make it u32 type, the first guess:String will be shadowed 
        // so the following code takes guess:u32 instead. 
        // let guess:u32 = guess.trim().parse().expect("please input a number");
        //  match keyword, which can be used like a C switch https://doc.rust-lang.org/rust-by-example/flow_control/match.html   
        let guess:u32 = match guess.trim().parse(){
            Ok(num_lol) => num_lol,
            Err(_) => { //use _ in () means we dont care things returned by parse and given to Err. You can use other things like err_msg. and println!({},err_msg);
                println!("you SHOULD input a number instead of a string",_);
                continue; // continue the loop{}
            },
        };
        match guess.cmp(&secret_number){
            Ordering::Less => println!("too small"),
            Ordering::Greater => println!("too big"),
            Ordering::Equal => {
                println!("You Win!");
                break; //break the loop{}
            }
        }
    }

    
}

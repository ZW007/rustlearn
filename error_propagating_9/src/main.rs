use std::io;
use std::io::Read;
use std::fs::File;
use std::error::Error;

fn main() {
    let result = read_string_from_file();
    //If the Result value is the Ok variant, unwrap will return the value inside the Ok. If the Result is the Err variant, unwrap will call the panic! macro for us
    // https://doc.rust-lang.org/book/ch09-02-recoverable-errors-with-result.html
    // println!("read result is {:?}", result.unwrap());
    // excpet is a better unwarp which allows error prompt
    println!("read result is {:?}", result.expect("No such a file, sorry"));
}

// fn read_string_from_file_old() -> Result<String, io::Error>{

//     let f = File::open("hello.txt");
//     let mut f_match = match f {
//         Ok(file) => file, // f_match = file
//         Err(e) => return Err(e), // Err(e) as the return type fits with -> Result<String, io::Error>
//     };
//     let mut s = String::new();
//     match f_match.read_to_string(&mut s){
//         Ok(a) => Ok(s),   // return Ok or Err to match, and then match{} (you can also do return match{}) returns Ok or Err to read_string_from_file() -> Result<,>
//         Err(e) => Err(e),    
//     }  
// }

// /*
//     ? works after an expression function that returns a Result<,> type, we can use ? to propogate the error, more concise. 
//     Also ? only works in a function such as read_string_from_file which returns a Result type
//     Return of ? opeartor : If the value of the Result is an Ok, the value inside the Ok will get returned from this expression, and the program will continue. If the value ///     is an Err, the Err will be returned from the whole function as if we had used the return keyword so the error value gets propagated to the calling code. 
       
//         let mut f = File::open("hello.txt")?; // if success return File, it not return Err(e_msg)
//     // so this is equivalent to :
//         let mut f = match File::open("hello.txt") {
//             Ok(file) => file,
//             Err(e) => return Err(e),
//         };
// */

fn read_string_from_file() -> Result<String, io::Error>{
    let mut f = File::open("hello.txt")?; // if success f = File, it unsuccessful directly return Err(e_msg) to calling function main()
    let mut s = String::new();
    f.read_to_string(&mut s)?; // read_to_string also returns a Result type, if Ok, then continue to next line and return Ok(s)
    Ok(s)     //Also you cannot replace the last two lines with  f.read_to_string(&mut s) bcz read_to_string returns  Result<usize,_> does not match  Result<String, _>
}

// The main function is special, and there are restrictions on what its return type must be. One valid return type for main is (), and conveniently, another valid return type is Result<T, E>, as shown here:
// fn main() -> Result<(),Box<dyn Error>>{
//     let f = File::open("./hello.txt");
//     return Ok(())
// }

use std::collections::HashMap;
fn main() {
    let text = "hello world , wonderful world";
    let mut word_countmap = HashMap::new();
    
    for word in text.split_whitespace() {
        let count = word_countmap.entry(word).or_insert(0); // if the entry unempty, 
                                                        //it will not inser 0, it will just a mutable returns referece to the value. So then we count + 1
                                                        // if entry empty, insert a value 0 and then count + 1 so it is 1
        *count += 1; // deference, becasue or_insert() returns a mutable reference to the value for the corresponding Entry key if that key exists
    }
    println!("count of each word in text {:#?}", word_countmap);

    // let v = vec![1, 2, 3, 4, 5];

    // let third: &i32 = &v[2];
    // println!("The third element is {}", third);

    // match v.get(2) {
    //     Some(third) => println!("The third element is {}", third),
    //     None => println!("There is no third element."),
    // }

    let mut v = vec![1, 2, 3, 4, 5];
    let first = & v[0];
    // v.push(6); // https://doc.rust-lang.org/book/ch08-01-vectors.html v.push(6) is an mutable borrow, while first = &v[0] is immutable borrow and println!(). So error
    // if you  let first = &mut v[0]; you can also not v.push() because otherwise two mutable borrows at the same time. Unless remove println! below so &mut first is ended
    println!("The first element is: {}", first);

    let mut v = vec![1, 2, 3, 4, 5];
    let mut v2 = vec![100, 2, 3, 4, 5];
    let  first_v = &mut v[0]; 
    let  first_v2 = &mut v2[0];
    *first_v = *first_v2;
    // println!("first_v is: {}", first_v);
    println!("The first element of vector v is: {}", v[0]);


    let mut v = vec![1, 2, 3, 4, 5];
    let mut v2 = vec![100, 2, 3, 4, 5];
    let mut first_v = &mut v[0]; 
    let  first_v2 = &mut v2[0];
    first_v = first_v2;
    // v.push(6); // https://doc.rust-lang.org/book/ch08-01-vectors.html
    println!("first_v is: {}", first_v);
    println!("The first element of vector v is: {}", v[0]);

    let mut v = vec![100, 32, 57];
    for i in &mut v {
        *i += 50;
    } 
    for i in &v {       // you should use &v instead of v because `v` ownership moved due to this implicit call to `.into_iter() in for loop
        println!("{}",i);
    }

 
}

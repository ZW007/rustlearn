

#[derive(Debug)] // Not mean the top of code, but put it right before the struct that you wanna apply println!()
struct Rectangle{
    width:u32,
    length:u32,
}

impl Rectangle {
    fn area(&self) ->u32{
        self.length*self.width
    }
    // Another useful feature of impl blocks is that we’re allowed to define functions within impl blocks that don’t take self as a parameter. 
    // These are called ASSOCIATED functions because they’re associated with the struct. They’re still functions, not methods, because they don’t have an instance of 
    // dthe struct to work with. You’ve already used the String::from() associated function.  see Rectangle::square(66) below as an example
    fn square(size:u32) -> Rectangle{
        Rectangle{
            width:size,
            length:size,
        }
    }

}

impl Rectangle {
    fn can_hold(&self,other_rectangle:&mut Rectangle) -> bool{
        self.length > other_rectangle.length && self.width > other_rectangle.width
    }
}

fn main() {
    let rect1 = (30,50);
    println!("Area is: {}",area_fn(rect1));

    let rect2= Rectangle{
        width:30,
        length:50,
    };

    println!("rect2 area is: {}",rect2.area());
    println!("rect2 struct is: {:#?}",rect2);

    println!("use associated function of the struct to get a square: {:#?}",Rectangle::square(66));

    let mut rect3 = Rectangle{
        width : 10,
        length : 40,
    };

    println!("rect2 can hold rect3? {}",rect2.can_hold(&mut rect3));
    rect3 = Rectangle{
        width : 10,
        length : 70,
    };

    println!("rect2 can hold rect3? {}",rect2.can_hold(&mut rect3));


}

fn area_fn(rect:(u32,u32))-> u32{
    rect.0*rect.1
}
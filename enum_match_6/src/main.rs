use std::fs::OpenOptions;


enum IpAddrKind {
    V4,
    V6,
}

struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

enum IPAddress { //can directly put tuple in enum instaed of using struct
    v4(u32,u32,u32,u32), 
    v6(String),
}

enum Message {
    Quit,
    // Move includes an anonymous struct inside it.
    Move {x:i32, y:i32,},
    Write(String),
    ChangeColor(i32, i32, i32),
}
// => the above is equivalent to <=
// struct QuitMessage; // unit struct
// struct MoveMessage {
//     x: i32,
//     y: i32,
// }
// struct WriteMessage(String); // tuple struct
// struct ChangeColorMessage(i32, i32, i32); // tuple struct 

impl Message {
    fn call(&self){
        //do something
    }
}

#[derive(Debug)]
enum State {
    Alabama,
    Alaska,
    // --snip--
}
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(State,),
}

fn main() {
    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1"),
    };
    
    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1"),
    };

    let home = IPAddress::v4(127,0,0,1);
    let loopback = IPAddress::v6(String::from("::1"));
    
    //enum Option<T>{Some<T>(),None,}
    let y: Option<i8> = Some(5);
    let x = Some(5);
    // let z = x+ y; //error, different type
    // let z = None; // must specify the type
    let z: Option<u32> = None;

   println!("The coin is: {}",value_in_cents(Coin::Quarter(State::Alabama)));

   let u8_value = 0u8;
   match u8_value {
       0 => println!("zero"),
       3 => println!("three"),
       5 => println!("five"),
       7 => println!("seven"),
       //The _ pattern will match any value.  becuase u8 type ranges from 0 - 255, cannot cover all matches 
       _ => (),
   }
   // if let is a special match, that only matches one case, you can add else to enable more matches though
   let some_u8_value = Some(3u8);

   if let Some(3) = some_u8_value{
        println!("three");
   } else {
       println!("others");
   }

}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(a_state) => {
            println!("State quarter from: {:#?}", a_state);
            25
        },
    }
}
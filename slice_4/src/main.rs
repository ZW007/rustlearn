
// String slice type which is &a_String[start..end]   
fn main() {
    let mut s = String::from("Hello World");
    let _first_word_zero = first_word(&s);
    // s.clear(); // Cannot clear here because _first_world_zero is an immutable reference to the s String, bcz the return type of first_world(s) is &s. 
                // s.clear() uses a mutalbe refernece to the s, bcz definition pub fn clear(&mut self)  Later println still uses the immutable refernce, _first_word_zero,
                // so this immutable reference is not out of the scope. https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html
    println!("first world is: {}", _first_word_zero);
    s.clear(); // it is OK to put here coz print! line is already done, before uses the mutable reference. so immutable reference is out of the scope

    let s_str = "Hello Zhen"; //s_str is a &str , slice type, while s and s_new are String type. 
    let _first_word_one = first_word_new(s_str);
    let s_new = String::from("Hello Rust");
    let _first_word_two = first_word_new(&s_new); // I should use slice type, but &String is also accpetable to this function becuase deref coercion. change &String to &str type
    let _first_word_three = first_word_new(&s_new[..]);
    println!("first world is: {}, {}, {}",_first_word_one,_first_word_two,_first_word_three);


}

fn first_word(s: &String) -> &str{   
    let byte = s.as_bytes(); 
    for (i,&item) in byte.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}

fn first_word_new(s: &str) -> &str{  // use s: &str , slice type as the parameter is more generic than using s: &String because you can also pass &str as the argument.
                                     // just like s_str = "hello zhen" which is a &str type that can be directly passed to this function but not allowed in first_world(s:&String) fn
    let byte = s.as_bytes();
    for (i,&item) in byte.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}

use std::fs::File;
use std::io::ErrorKind;

fn main(){
    let f = File::open("hello.txt");

    let f2 = match f {
        Result::Ok(file_zhen) => file_zhen,  // Ok and Err are in prelude just like Some and None of option<T>, so indeed no need to add Result::Ok and Result::Err 
        Result::Err(whatever_error) => match whatever_error.kind() { //whatever_error.kind()
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(newcreated_file_zhen) => newcreated_file_zhen,
                Err(whatever_error_2) => panic!("file you want open NotFound, and had error in creating a new file {}", whatever_error_2),
            }  
            other_error_zhen => {
                panic!("Problem in opening the file:{:?}",other_error_zhen);  // cannot use print because print returns `()` but let f2 = expects a file return
            }
        },
    };
}




// fn main() {
//     let f = File::open("hello.txt");

//     let f = match f {
//         Ok(file) => file,
//         Err(error) => panic!("Problem opening the file: {:?}", error),
//     };
// }
// unwrap() will return the value inside the Ok. If the Result is the Err variant, unwrap will call the panic! macro for us. 
// The following code has the same functionality as the above one
// fn main() {
//     let f = File::open("hello.txt").unwrap();
// }
// expect, which is similar to unwrap, lets us also choose the panic! error message. 
// fn main(){
//     let f = File::open("hello.txt").expect("Problem opening the file");  // f type is file, not Result<File, error>
// }